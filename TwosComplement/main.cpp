#include <iostream>
#include <bitset>
#include <string>

using namespace std;

// Function to convert decimal to binary string
string decimalToBinary(int num, int numBits) {
    return bitset<32>(num).to_string().substr(32 - numBits);
}

// Function to convert binary string to decimal
int binaryToDecimal(string bin) {
    return stoi(bin, nullptr, 2);
}

// Function to perform two's complement for negative numbers
string twosComplement(string bin) {
    // Flip bits
    for (char& c : bin) {
        c = (c == '0') ? '1' : '0';
    }
    // Add 1
    int carry = 1;
    for (int i = bin.size() - 1; i >= 0; --i) {
        if (bin[i] == '1' && carry == 1) {
            bin[i] = '0';
        }
        else if (bin[i] == '0' && carry == 1) {
            bin[i] = '1';
            carry = 0;
        }
    }
    return bin;
}

int main() {
    int num = -5;
    int numBits = 11;

    // Convert decimal to binary
    string binary = (num >= 0) ? decimalToBinary(num, numBits) : twosComplement(decimalToBinary(-num, numBits));

    cout << "Binary representation of " << num << " is: " << binary << endl;

    // Convert binary to decimal
    int decimal = (binary[0] == '0') ? binaryToDecimal(binary) : -binaryToDecimal(twosComplement(binary));

    cout << "Decimal representation of " << binary << " is: " << decimal << endl;

    return 0;
}
